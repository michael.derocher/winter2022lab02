public class MethodsTest {
	
	public static void main(String[] args) {
		
		int x = methodNoInputReturnInt();
		
		methodOneInputNoReturn(10);
		methodOneInputNoReturn(x);
		methodOneInputNoReturn(x + 50);
		methodTwoInputNoReturn(10, 5.0);
		double y = sumSquareRoot(6, 3);
		System.out.println(y);
		
		String s1 = "hello";
		String s2 = "goodbye";
		
		System.out.println(s1.length());
		System.out.println(s2.length());
		
		SecondClass sc = new SecondClass();
		
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}
	public static void methodNoInputNoReturn() {
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(int x) {
		System.out.println("Inside the method one input no return: " + x);
	}
	public static void methodTwoInputNoReturn(int x, double y) {
		System.out.println("Inside the method two inputs no return: " + x + " " + y);
	}
	public static int methodNoInputReturnInt() {
		System.out.println("Inside the method no inputs return int");
		return 6;
	}
	public static double sumSquareRoot(int x, int y) {
		int z = x + y;
		return Math.sqrt(z);
	}
	
	
}