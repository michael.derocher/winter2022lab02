public class PartThree {
	
	public static void main(String[] args){
		
		java.util.Scanner reader = new java.util.Scanner(System.in);
		
		System.out.println("What is the length of the square?");
		double sqLength = reader.nextDouble();
		
		System.out.println("What is the length of the rectangle?");
		double rectLength = reader.nextDouble();
		
		System.out.println("What is the height of the rectangle?");
		double rectHeight = reader.nextDouble();
		
		AreaComputations ac = new AreaComputations();
		
		System.out.println(AreaComputations.areaSquare(sqLength));
		System.out.println(ac.areaRectangle(rectLength, rectHeight));
		
	}
	
	
	
}